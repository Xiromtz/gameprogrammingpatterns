﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breed : ScriptableObject
{
    [Header("Health")]
    public int startingHealth = 100;

    [Header("Attack")]
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

    [Header("SFX")]
    public AudioClip hurtClip;
    public AudioClip deathClip;

    [Header("Score")]
    public int scoreValue = 10;
}
