﻿using System.Collections;
using System.Collections.Generic;
using CompleteProject;
using UnityEngine;

public class Monster : MonoBehaviour
{
    public Breed breed;

    private EnemyHealth_ALT enemyHealth;
    private EnemyAttack_ALT enemyAttack;

	private bool deathTriggered = false;

    void Awake()
    {
        enemyHealth = gameObject.AddComponent<EnemyHealth_ALT>();
        enemyAttack = gameObject.AddComponent<EnemyAttack_ALT>();
        gameObject.AddComponent<EnemyMovement_ALT>();

        enemyHealth.startingHealth = breed.startingHealth;
		enemyHealth.currentHealth = breed.startingHealth;
        enemyHealth.hurtClip = breed.hurtClip;
        enemyHealth.deathClip = breed.deathClip;
        enemyHealth.scoreValue = breed.scoreValue;
        
        enemyAttack.attackDamage = breed.attackDamage;
        enemyAttack.timeBetweenAttacks = breed.timeBetweenAttacks;
    }

	void Update()
	{
		if (enemyHealth.isDead && !deathTriggered) 
		{
			deathTriggered = true;
			EventQueue.queue (breed.name.ToLower () + "death", 1);
		}
	}
}
