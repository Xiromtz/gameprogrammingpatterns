﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventMessage
{
	public int eventHash;
	public int count;
}

public class EventQueue : MonoBehaviour 
{
	public EventReceiver[] receivers;

	static int MAX_PENDING = 100;
	static EventMessage[] pending = new EventMessage[MAX_PENDING];

	static int head = 0, tail = 0;

	public static void queue(string eventName, int count)
	{
		if ((tail+1)%MAX_PENDING == head)
			return;
		
		int eventHash = Animator.StringToHash (eventName);
		pending [tail] = new EventMessage ();
		pending [tail].eventHash = eventHash;
		pending [tail].count = count;
		tail = (tail+1)%MAX_PENDING;
	}

	// Update is called once per frame
	void Update () 
	{
		if (head != tail) 
		{
			foreach (EventReceiver receiver in receivers)
			{
				receiver.receive(pending[head]);
			}
			head = (head+1) % MAX_PENDING;
		}
	}
}
