﻿using UnityEngine;

public abstract class EventReceiver : MonoBehaviour
{
	public abstract void receive(EventMessage msg);
}
