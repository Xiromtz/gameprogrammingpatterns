﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementManager : EventReceiver
{
	public List<Achievement> achievements;
	public Text achievementText;

	private bool isAchievementShowing = false;
	private Animator achievementAnimator;
	private Dictionary<Achievement, int> achievementProgression = new Dictionary<Achievement, int>();

	void Awake()
	{
		foreach (Achievement achievement in achievements) 
		{
			achievementProgression.Add (achievement, 0);
		}
		achievementAnimator = GetComponent<Animator> ();
	}

	public override void receive(EventMessage msg)
	{
		foreach (Achievement achievement in achievements) 
		{
			if (Animator.StringToHash (achievement.eventName) == msg.eventHash) 
			{
				achievementProgression [achievement] += msg.count;
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isAchievementShowing) 
		{
			foreach (KeyValuePair<Achievement, int> entry in achievementProgression) 
			{
				if (entry.Value >= entry.Key.achievementTriggerEventCount) 
				{
					isAchievementShowing = true;
					StartCoroutine (showAchievement(entry.Key.text));
					achievementProgression.Remove (entry.Key);
					achievements.Remove (entry.Key);
					break;
				}
			}
		}
	}

	private IEnumerator showAchievement(string achievement)
	{
		achievementText.text = achievement;
		achievementAnimator.SetBool ("IsOpen", true);
		while (!achievementAnimator.GetCurrentAnimatorStateInfo (0).IsName ("Base.AchievementOpen")) {
			yield return null;
		}
		yield return new WaitForSeconds (2);
		StartCoroutine (hideAchievement ());
	}

	private IEnumerator hideAchievement()
	{
		achievementAnimator.SetBool ("IsOpen", false);
		while (!achievementAnimator.GetCurrentAnimatorStateInfo (0).IsName ("Base.AchievementClose")) 
		{
			yield return null;
		}
		isAchievementShowing = false;
	}
}
