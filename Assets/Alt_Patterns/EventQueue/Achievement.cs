﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Achievement : ScriptableObject 
{
	public string text;
	public string eventName;
	public int achievementTriggerEventCount;
}
