﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public GameObject pooledObject;
    public int pooledAmount = 20;
    public bool willGrow = true;

    public List<GameObject> pool;
    
	void Start ()
	{
	    pool = new List<GameObject>();
	    for (int i = 0; i < pooledAmount; i++)
	    {
	        AddNewInstance();
	    }
	}

    private GameObject AddNewInstance()
    {
        GameObject instance = Instantiate(pooledObject);
        instance.SetActive(false);
        pool.Add(instance);
        return instance;
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            GameObject instance = pool[i];
            if (!instance.activeInHierarchy)
            {
                return instance;
            }
        }

        if (willGrow)
        {
            GameObject newInstance = AddNewInstance();
            return newInstance;
        }
        
        return null;
    }
}
