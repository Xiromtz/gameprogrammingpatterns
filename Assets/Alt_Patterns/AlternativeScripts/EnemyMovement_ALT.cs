﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
    public class EnemyMovement_ALT : MonoBehaviour
    {
        Transform player;               // Reference to the player's position.
        PlayerHealth_ALT playerHealth;      // Reference to the player's health.
        EnemyHealth_ALT enemyHealth;        // Reference to this enemy's health.
        UnityEngine.AI.NavMeshAgent nav;               // Reference to the nav mesh agent.


        void Awake ()
        {
            // Set up the references.
            player = GameObject.FindGameObjectWithTag ("Player").transform;
            playerHealth = player.GetComponent <PlayerHealth_ALT> ();
            enemyHealth = GetComponent <EnemyHealth_ALT> ();
            nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
        }


        void Update ()
        {
            // If the enemy and the player have health left...
            if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
            {
                // ... set the destination of the nav mesh agent to the player.
                nav.SetDestination (player.position);
            }
            // Otherwise...
            else
            {
                // ... disable the nav mesh agent.
                nav.enabled = false;
            }
        }
    }
}