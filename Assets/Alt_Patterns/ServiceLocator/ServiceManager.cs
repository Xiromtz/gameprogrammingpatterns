﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceManager : MonoBehaviour
{

    public bool enableLogging = true;
    
    void Awake()
    {
        AudioLocator.initialize();
        GameObject audioInstance = new GameObject("AudioService");
        Audio audioService = audioInstance.AddComponent<Audio>();

        if (enableLogging)
        {
            IAudio loggingService = new LoggedAudio(audioService);
            AudioLocator.provide(loggingService);
        }
        else
            AudioLocator.provide(audioService);
    }
}
