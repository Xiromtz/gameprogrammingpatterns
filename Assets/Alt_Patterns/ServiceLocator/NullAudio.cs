﻿using UnityEngine;

public class NullAudio : IAudio {
    public void playSound(AudioClip clip, bool looping = false)
    {
        
    }

    public void stopSound(AudioClip clip)
    {
       
    }

    public void stopAllSounds()
    {
        
    }
}
