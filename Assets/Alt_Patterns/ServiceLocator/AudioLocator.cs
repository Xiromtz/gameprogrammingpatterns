﻿using UnityEngine;

public class AudioLocator
{
    private static IAudio service;
    private static NullAudio nullService;

    public static void initialize()
    {
        nullService = new NullAudio();
        service = nullService;
    }
    
    public static IAudio getAudio()
    {
        return service;
    }

    public static void provide(IAudio service)
    {
        if (service == null)
        {
            service = nullService;
            return;
        }
        AudioLocator.service = service;
    }
}