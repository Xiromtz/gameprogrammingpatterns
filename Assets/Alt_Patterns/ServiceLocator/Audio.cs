﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour, IAudio
{
    private List<AudioSource> currentSources;

    void Awake()
    {
        currentSources = new List<AudioSource>();
    }

    void Update()
    {
        for (int i = 0; i < currentSources.Count; i++)
        {
            if (!currentSources[i].isPlaying)
            {
                Destroy(currentSources[i]);
                currentSources.RemoveAt(i);
            }
        }
    }
    
    public void playSound(AudioClip clip, bool looping = false)
    {
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = clip;
        source.loop = looping;
        source.Play();
        currentSources.Add(source);
    }

    public void stopSound(AudioClip clip)
    {
        for (int i = 0; i < currentSources.Count; i++)
        {
            if (currentSources[i].clip == clip)
            {
                currentSources[i].Stop();
            }
        }
    }

    public void stopAllSounds()
    {
        for (int i = 0; i < currentSources.Count; i++)
        {
            currentSources[i].Stop();
        }
    }
}
