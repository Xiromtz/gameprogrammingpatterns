﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAudio : MonoBehaviour
{
    public AudioClip bgMusic;
	// Use this for initialization
	void Start ()
	{
	    IAudio service = AudioLocator.getAudio();
	    service.playSound(bgMusic, true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
