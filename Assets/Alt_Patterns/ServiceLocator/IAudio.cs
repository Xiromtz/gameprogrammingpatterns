﻿using UnityEngine;

public interface IAudio
{
    void playSound(AudioClip clip, bool looping=false);
    void stopSound(AudioClip clip);
    void stopAllSounds();
}
