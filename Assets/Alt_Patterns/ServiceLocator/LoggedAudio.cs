﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoggedAudio : IAudio
{
    private IAudio _wrapped;
    
    public LoggedAudio(IAudio wrapped)
    {
        _wrapped = wrapped;
    }
    
    public void playSound(AudioClip clip, bool looping = false)
    {
        Debug.Log("play sound " + clip.name);
        _wrapped.playSound(clip, looping);
    }

    public void stopSound(AudioClip clip)
    {
        Debug.Log("stop sound " + clip.name);
        _wrapped.stopSound(clip);
    }

    public void stopAllSounds()
    {
        Debug.Log("stop all sounds");
        _wrapped.stopAllSounds();
    }
}
