﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AchievementAsset
{
	[MenuItem("Assets/Create/Achievement")]
	public static void CreateAsset()
	{
		ScriptableObjectUtility.CreateAsset<Achievement>();
	}
}
