﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BreedAsset {

    [MenuItem("Assets/Create/Breed")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<Breed>();
    }
}
